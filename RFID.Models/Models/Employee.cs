﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace RFID.Models
{
    public class Employee : INotifyPropertyChanged
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int cardId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }

        public int CardId
        {
            get
            {
                return cardId;
            }
            set
            {
                cardId = value;
                OnPropertyChanged("CardId");
            }


        }
        
    }
}
