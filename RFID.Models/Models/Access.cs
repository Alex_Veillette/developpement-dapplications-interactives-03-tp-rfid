﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace RFID.Models
{
    public class Access : INotifyPropertyChanged
    {
        public int cardId { get; set; }
        public int roomId { get; set; }
        public string begin { get; set; }
        public string end { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int CardId
        {
            get
            {
                return cardId;
            }
            set
            {
                cardId = value;
                OnPropertyChanged("CardId");
            }
        }

        public int RoomId
        {
            get
            {
                return roomId;
            }
            set
            {
                roomId = value;
                OnPropertyChanged("RoomId");
            }
        }

        public string Begin
        {
            get
            {
                return begin;
            }
            set
            {
                begin = value;
                OnPropertyChanged("Begin");
            }
        }

        public string End
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
                OnPropertyChanged("End");
            }
        }


    }
}
