﻿using RFID.Models;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using TP_01.Data;

namespace TP_01.ViewModels
{
    public class RoomViewModel : ViewModelBase
    {
        private string id_txt;
        private string description_txt;

        MainViewModel mainViewModel;
        public ObservableCollection<Room> Rooms { get; set; }
        Room _room;

        public ICommand AddRoomCommand { get; set; }
        public ICommand CancelRoomCommand { get; set; }

        public RoomViewModel(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;

            DataAccess dataAccess = new DataAccess();
            Rooms = new ObservableCollection<Room>(dataAccess.GetRooms());

            AddRoomCommand = new DelegateCommand(AddMethod, CanAddMethod);
            CancelRoomCommand = new DelegateCommand(CancelMethod, CanCancelMethod);
        }

        //====================
        //  Execute
        //====================
        public void AddMethod(object parameter)
        {
            string winName = parameter as string;
            //  Convert
            int idNumber;
            if (Int32.TryParse(id_txt, out idNumber))
            {
                Room newRoom = new Room() { id = idNumber, description = description_txt };
                Rooms.Add(newRoom);
                mainViewModel.FillList();
                OnPropertyChanged("Room");
                mainViewModel.OpenCloseWindowMethod(true, winName);

                id_txt = "";
                description_txt = "";
            }
        }

        public void CancelMethod(object parameter)
        {
            string winName = parameter as string;
            mainViewModel.OpenCloseWindowMethod(true, winName);
        }
        //====================

        //====================
        //  Can Execute
        //====================

        public bool CanAddMethod(object parameter)
        {
            return true;
        }

        public bool CanCancelMethod(object parameter)
        {
            return true;
        }

        //====================

        public string Id_txt
        {
            get
            {
                return id_txt;
            }
            set
            {
                id_txt = value;
            }
        }

        public string Description_txt
        {
            get
            {
                return description_txt;
            }
            set
            {
                description_txt = value;
            }
        }
    }
}
