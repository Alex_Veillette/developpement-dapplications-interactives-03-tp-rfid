﻿using RFID.Models;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using TP_01.Data;

namespace TP_01.ViewModels
{
    
    public class EmployeeViewModel : ViewModelBase
    {
        private string id_txt;
        private string firstName_txt;
        private string lastName_txt;
        private string cardId_txt;

        MainViewModel mainViewModel;
        public ObservableCollection<Employee> Employees { get; set; }
        Employee _employee;

        public ICommand AddEmployeeCommand { get; set; }
        public ICommand CancelEmployeeCommand { get; set; }

        public EmployeeViewModel(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;

            DataAccess dataAccess = new DataAccess();
            Employees = new ObservableCollection<Employee>(dataAccess.GetEmployees());

            AddEmployeeCommand = new DelegateCommand(AddMethod, CanAddMethod);
            CancelEmployeeCommand = new DelegateCommand(CancelMethod, CanCancelMethod);
        }

        //====================
        //  Execute
        //====================
        public void AddMethod(object parameter)
        {
            string winName = parameter as string;

            //  Convert
            int idNumber, cardIdNumber;
            if((Int32.TryParse(id_txt, out idNumber)) && (Int32.TryParse(cardId_txt, out cardIdNumber)))
            {
                Employee newEmployee = new Employee() { id = idNumber, firstName = firstName_txt, lastName = lastName_txt, cardId = cardIdNumber };
                Employees.Add(newEmployee);
                mainViewModel.FillList();
                OnPropertyChanged("Employee");
                mainViewModel.OpenCloseWindowMethod(true, winName);

                id_txt = "";
                firstName_txt = "";
                lastName_txt = "";
                cardId_txt = "";
            }
        }

        

        public void CancelMethod(object parameter)
        {
            string winName = parameter as string;
            mainViewModel.OpenCloseWindowMethod(true, winName);
        }
        //====================

        //====================
        //  Can Execute
        //====================

        public bool CanAddMethod(object parameter)
        {
            return true;
        }

        public bool CanCancelMethod(object parameter)
        {
            return true;
        }

        //====================

        public string Id_txt
        {
            get
            {
                return id_txt;
            }
            set
            {
                id_txt = value;
            }
        }

        public string FirstName_txt
        {
            get
            {
                return firstName_txt;
            }
            set
            {
                firstName_txt = value;
            }
        }

        public string LastName_txt
        {
            get
            {
                return lastName_txt;
            }
            set
            {
                lastName_txt = value;
            }
        }

        public string CardId_txt
        {
            get
            {
                return cardId_txt;
            }
            set
            {
                cardId_txt = value;
            }
        }
    }
}
