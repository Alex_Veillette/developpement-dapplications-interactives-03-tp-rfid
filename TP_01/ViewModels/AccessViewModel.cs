﻿using System;
using RFID.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using TP_01.Data;
using System.Diagnostics;

namespace TP_01.ViewModels
{
    public class AccessViewModel : ViewModelBase
    {
        private string cardId_txt;
        private string roomId_txt;
        private string wantedEmpId;
        private string firstName;
        private string lastName;
        private string debut_hr_txt;
        private string debut_min_txt;
        private string fin_hr_txt;
        private string fin_min_txt;
        

        MainViewModel mainViewModel;
        
        public ObservableCollection<Access> Access { get; set; }
        public ObservableCollection<Employee> EmployeeList { get; set; }

        Access _access;

        public ICommand AddAccessCommand { get; set; }
        public ICommand CancelAccessCommand { get; set; }

        public AccessViewModel(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;

            DataAccess dataAccess = new DataAccess();

            Access = new ObservableCollection<Access>(dataAccess.GetAccess());

            AddAccessCommand = new DelegateCommand(AddMethod, CanAddMethod);
            CancelAccessCommand = new DelegateCommand(CancelMethod, CanCancelMethod);
        }

        //====================
        //  Execute
        //====================
        public void AddMethod(object parameter)
        {
            string winName = parameter as string;

            int cardIdNumber, roomIdNumber;
            if ((Int32.TryParse(cardId_txt, out cardIdNumber)) && (Int32.TryParse(roomId_txt, out roomIdNumber)))
            {
                Access newAccess = new Access() { cardId = cardIdNumber, roomId = roomIdNumber, begin = (debut_hr_txt + "h" + debut_min_txt), end = (fin_hr_txt + "h" + fin_min_txt) };
                Access.Add(newAccess);
                mainViewModel.FillList();
                OnPropertyChanged("Access");
                mainViewModel.OpenCloseWindowMethod(true, winName);

                cardId_txt = "";
                roomId_txt = "";
                debut_hr_txt = "";
                debut_min_txt = "";
                fin_hr_txt = "";
                fin_min_txt = "";
            }
        }

        public void CancelMethod(object parameter)
        {
            string winName = parameter as string;
            mainViewModel.OpenCloseWindowMethod(true, winName);
        }
        //====================

        //====================
        //  Can Execute
        //====================

        public bool CanAddMethod(object parameter)
        {
            return true;
        }

        public bool CanCancelMethod(object parameter)
        {
            return true;
        }

        //====================

        public string CardId_txt
        {
            get
            {
                return cardId_txt;
            }
            set
            {
                cardId_txt = value;
            }
        }

        public string RoomId_txt
        {
            get
            {
                return roomId_txt;
            }
            set
            {
                roomId_txt = value;
            }
        }

        public string DebutHr_txt
        {
            get
            {
                return debut_hr_txt;
            }
            set
            {
                debut_hr_txt = value;
            }
        }

        public string DebutMin_txt
        {
            get
            {
                return debut_min_txt;
            }
            set
            {
                debut_min_txt = value;
            }
        }

        public string FinHr_txt
        {
            get
            {
                return fin_hr_txt;
            }
            set
            {
                fin_hr_txt = value;
            }
        }

        public string FinMin_txt
        {
            get
            {
                return fin_min_txt;
            }
            set
            {
                fin_min_txt = value;
            }
        }
    }
}
