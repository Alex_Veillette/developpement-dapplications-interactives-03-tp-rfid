﻿using RFID.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace TP_01.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        //======================
        //  Attributes
        //======================
        int tab, iteratorEmployee, iteratorRoom, iteratorAccess;
        bool isEnabledEmp, isEnabledRoom, isEnabledAccess;
        //======================

        //======================
        //  Windows
        //======================
        AjoutEmploye ajoutEmploye;
        AjoutLocal ajoutLocal;
        AjoutAccesLocaux ajoutAccesLocaux;
        //======================

        EmployeeViewModel employeeViewModel;
        RoomViewModel roomViewModel;
        AccessViewModel accessViewModel;

        ObservableCollection<Employee> employeeList;
        ObservableCollection<Room> roomList;
        ObservableCollection<Access> accessList;

        //======================

        //======================
        //  Commands
        //======================
        public ICommand OpenAddWindowCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand NextCommand { get; set; }
        public ICommand PreviousCommand { get; set; }
        public ICommand ShutDownCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        //======================

        public MainViewModel()
        {
            //======================
            //  ViewModels
            //======================
            employeeViewModel = new EmployeeViewModel(this);
            roomViewModel = new RoomViewModel(this);
            accessViewModel = new AccessViewModel(this);
            //======================

            //======================
            //  Commands
            //======================
            OpenAddWindowCommand = new DelegateCommand(OpenAddWindowMethod, CanOpenWindowCommand);
            EditCommand = new DelegateCommand(EditMethod, CanEditCommand);
            NextCommand = new DelegateCommand(NextMethod, CanNextCommand);
            PreviousCommand = new DelegateCommand(PreviousMethod, CanPreviousCommand);
            DeleteCommand = new DelegateCommand(RemoveMethod, CanDeleteCommand);
            ShutDownCommand = new DelegateCommand(ShutDownMethod, CanShutDownCommand);
            //======================

            //======================
            //  Lists
            //======================
            employeeList = new ObservableCollection<Employee>();
            roomList = new ObservableCollection<Room>();
            accessList = new ObservableCollection<Access>();

            FillList();
            //======================

            iteratorEmployee = 0;
            iteratorRoom = 0;
            iteratorAccess = 0;
        }

        //======================
        //  Executes
        //======================

        public void NextMethod(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    if (iteratorEmployee < employeeViewModel.Employees.Count - 1)
                    {
                        iteratorEmployee++;
                        OnPropertyChanged("Employee");
                        OnPropertyChanged("Access");
                    }
                    break;

                case 1:
                    if (iteratorRoom < roomViewModel.Rooms.Count - 1)
                    {
                        iteratorRoom++;
                        OnPropertyChanged("Room");
                    }
                    break;

                case 2:
                    if (iteratorAccess < accessViewModel.Access.Count - 1)
                    {
                        iteratorAccess++;
                        OnPropertyChanged("AccessEmployee");
                    }
                    break;
            }


        }

        public void PreviousMethod(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    if (iteratorEmployee > 0)
                    {
                        iteratorEmployee--;
                        OnPropertyChanged("Employee");
                        OnPropertyChanged("Access");
                    }
                    break;

                case 1:
                    if (iteratorRoom > 0)
                    {
                        iteratorRoom--;
                        OnPropertyChanged("Room");
                    }
                    break;

                case 2:
                    if (iteratorAccess > 0)
                    {
                        iteratorAccess--;
                        OnPropertyChanged("AccessEmployee");
                    }
                    break;
            }
        }

        public void OpenCloseWindowMethod(bool winState, string winName)
        {
            if (winState == true)
            {
                switch (winName)
                {
                    case "window_employee":
                        ajoutEmploye.Close();
                        ajoutEmploye = null;
                        break;

                    case "window_room":
                        ajoutLocal.Close();
                        ajoutLocal = null;
                        break;

                    case "window_access":
                        ajoutAccesLocaux.Close();
                        ajoutAccesLocaux = null;
                        break;
                }
            }
            else
            {
                switch (winName)
                {
                    case "window_employee":
                        ajoutEmploye = new AjoutEmploye();
                        ajoutEmploye.Show();
                        break;

                    case "window_room":
                        ajoutLocal = new AjoutLocal();
                        ajoutLocal.Show();
                        break;

                    case "window_access":
                        ajoutAccesLocaux = new AjoutAccesLocaux();
                        ajoutAccesLocaux.Show();
                        break;
                }
            }
        }

        public void OpenAddWindowMethod(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    OpenCloseWindowMethod(false, "window_employee");
                    ajoutEmploye.DataContext = employeeViewModel;
                    break;

                case 1:
                    OpenCloseWindowMethod(false, "window_room");
                    ajoutLocal.DataContext = roomViewModel;
                    break;

                case 2:
                    OpenCloseWindowMethod(false, "window_access");
                    ajoutAccesLocaux.DataContext = accessViewModel;
                    break;
            }
        }

        public void EditMethod(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    OpenCloseWindowMethod(false, "window_edit_employee");
                    ajoutEmploye.DataContext = employeeViewModel;
                    break;

                case 1:
                    OpenCloseWindowMethod(false, "window_edit_room");
                    ajoutLocal.DataContext = roomViewModel;
                    break;

                case 2:
                    OpenCloseWindowMethod(false, "window_edit_access");
                    ajoutAccesLocaux.DataContext = accessViewModel;
                    break;
            }
        
        }

        public void RemoveMethod(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    employeeViewModel.Employees.RemoveAt(iteratorEmployee);
                    if(iteratorEmployee > 0)
                    {
                        iteratorEmployee--;
                    }
                    OnPropertyChanged("Employee");
                    OnPropertyChanged("Access");
                    break;

                case 1:
                    roomViewModel.Rooms.RemoveAt(iteratorRoom);
                    if (iteratorRoom > 0)
                    {
                        iteratorRoom--;
                    }
                    OnPropertyChanged("Room");
                    break;

                case 2:
                    accessViewModel.Access.RemoveAt(iteratorAccess);
                    if (iteratorAccess > 0)
                    {
                        iteratorAccess--;
                        OnPropertyChanged("Access");
                    }
                    break;
            }
            FillList();
        }

        public void ShutDownMethod(object parameter)
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void FillList()
        {
            employeeList.Clear();
            roomList.Clear();
            accessList.Clear();

            foreach(Employee employee in employeeViewModel.Employees)
            {
                employeeList.Add(employee);
            }

            foreach(Room room in roomViewModel.Rooms)
            {
                roomList.Add(room);
            }

            foreach(Access access in accessViewModel.Access)
            {
                accessList.Add(access);
            }


        }
        //=====================

        //=====================
        //  Can Execute
        //=====================
        private bool CanOpenWindowCommand(object parameter)
        {
            return true;
        }

        public bool CanEditCommand(object parameter)
        {
            return true;
        }
        private bool CanNextCommand(object parameter)
        {
            tab = (int)parameter;
            switch(tab)
            {
                case 0:
                    return employeeViewModel.Employees[iteratorEmployee] != null;
                    break;

                case 1:
                    return roomViewModel.Rooms[iteratorRoom] != null;
                    break;

                case 2:
                    return accessViewModel.Access[iteratorAccess] != null;
                    break;
            }

            return false;
        }

        private bool CanPreviousCommand(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    return employeeViewModel.Employees[iteratorEmployee] != null;
                    break;

                case 1:
                    return roomViewModel.Rooms[iteratorRoom] != null;
                    break;

                case 2:
                    return accessViewModel.Access[iteratorAccess] != null;
                    break;
            }

            return false;
        }

        private bool CanDeleteCommand(object parameter)
        {
            tab = (int)parameter;
            switch (tab)
            {
                case 0:
                    if (employeeList.Count() > 0)
                    {
                        return true;
                    }
                    break;

                case 1:
                    if (roomList.Count() > 0)
                    {
                        return true;
                    }
                    break;

                case 2:
                    if (accessList.Count() > 0)
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        public bool CanShutDownCommand(object parameter)
        {
            return true;
        }

        //=====================

        //=====================
        //  Lists
        //=====================
        public ObservableCollection<Employee> EmployeesList
        {
            get
            {
                return employeeList;
            }
            set
            {
                employeeList = value;
            }
        }

        public ObservableCollection<Room> RoomsList
        {
            get
            {
                return roomList;
            }
            set
            {
                roomList = value;
            }
        }

        public ObservableCollection<Access> AccessList
        {
            get
            {
                return accessList;
            }
            set
            {
                accessList = value;
            }
        }

        //=====================

        //=====================
        //  Get Specific
        //=====================
        public Employee Employee
        {
            get
            {
                try { return employeeViewModel.Employees[iteratorEmployee]; }
                catch { return null; }
            }
        }

        public Room Room
        {
            get { return roomViewModel.Rooms[iteratorRoom]; }
        }

        public Access AccessEmployee
        {
            get
            {
                try { return accessViewModel.Access[iteratorAccess]; }
                catch { return null; }
            }
        }

        public IEnumerable<Access> Access
        {
            get
            {
                try { return accessViewModel.Access.Where(access => access.CardId == employeeViewModel.Employees[iteratorEmployee].CardId); }
                catch { return null; }
            }
        }

        //=====================
    }
}
