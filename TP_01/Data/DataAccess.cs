﻿using RFID.Models;
using System.Collections.Generic;
using System.Linq;

namespace TP_01.Data
{
    public class DataAccess
    {
        List<Employee> employeesList = new List<Employee>();
        List<Room> roomsList = new List<Room>();
        List<Access> accessList = new List<Access>();

        Employee _employee;
        Room _room;
        Access _access;

        public DataAccess()
        {
            CreateEmployees();
            CreateRooms();
            CreateAccess();
        }

        public void CreateEmployees()
        {
            _employee = new Employee() { id = 1, firstName = "Wong", lastName = "Tong", cardId = 1 };
            employeesList.Add(_employee);
            _employee = new Employee() { id = 2, firstName = "Wing", lastName = "Ting", cardId = 2 };
            employeesList.Add(_employee);
            _employee = new Employee() { id = 3, firstName = "Wang", lastName = "Tang", cardId = 3 };
            employeesList.Add(_employee);
            _employee = new Employee() { id = 4, firstName = "Sam", lastName = "Touche", cardId = 4 };
            employeesList.Add(_employee);
        }

        public void CreateRooms()
        {
            _room = new Room() { Id = 1, Description = "Salle de caméras" };
            roomsList.Add(_room);
            _room = new Room() { Id = 2, Description = "Salle de repos" };
            roomsList.Add(_room);
            _room = new Room() { Id = 3, Description = "Spa" };
            roomsList.Add(_room);
            _room = new Room() { Id = 4, Description = "Salle à manger" };
            roomsList.Add(_room);
            _room = new Room() { Id = 5, Description = "Salle de rangement" };
            roomsList.Add(_room);
        }

        public void CreateAccess()
        {
            _access = new Access() { RoomId = 1, CardId = 1, Begin = "12h00", End = "15h00", cardId = employeesList.ElementAt(0).CardId, roomId = roomsList.ElementAt(0).Id};
            accessList.Add(_access);
            _access = new Access() { RoomId = 2, CardId = 1, Begin = "12h30", End = "15h10", cardId = employeesList.ElementAt(0).CardId, roomId = roomsList.ElementAt(1).Id };
            accessList.Add(_access);
            _access = new Access() { RoomId = 3, CardId = 2, Begin = "6h00", End = "13h00", cardId = employeesList.ElementAt(1).CardId, roomId = roomsList.ElementAt(2).Id };
            accessList.Add(_access);
            _access = new Access() { RoomId = 4, CardId = 2, Begin = "3h00", End = "6h00", cardId = employeesList.ElementAt(1).CardId, roomId = roomsList.ElementAt(3).Id };
            accessList.Add(_access);
            _access = new Access() { RoomId = 4, CardId = 3, Begin = "12h00", End = "13h00", cardId = employeesList.ElementAt(2).CardId, roomId = roomsList.ElementAt(3).Id };
            accessList.Add(_access);
            _access = new Access() { RoomId = 5, CardId = 4, Begin = "12h00", End = "15h00", cardId = employeesList.ElementAt(3).CardId, roomId = roomsList.ElementAt(4).Id };
            accessList.Add(_access);
        }

        public List<Employee> GetEmployees()
        {
            return employeesList;
        }

        public List<Room> GetRooms()
        {
            return roomsList;
        }

        public List<Access> GetAccess()
        {
            return accessList;
        }
    }
}
