﻿using System.Windows;
using TP_01.ViewModels;

namespace TP_01
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //DataProvider.Initialize();
            MainWindow mainWindow = new MainWindow();
            MainViewModel mainViewModel = new MainViewModel();

            mainWindow.DataContext = mainViewModel;
            mainWindow.Title = "RFID";
            mainWindow.Show();
        }
    }
}
